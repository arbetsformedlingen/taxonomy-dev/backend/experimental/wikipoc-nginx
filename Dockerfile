FROM nginx:1.27.0

ENV NGINX_VERSION=1.27.0

# Add application sources
# ADD nginx.conf "${NGINX_CONF_PATH}"

ADD nginx.conf /etc/nginx/conf.d/my-site.conf.template 
ADD nginx-default-cfg/*.conf "${NGINX_DEFAULT_CONF_PATH}" 
ADD nginx-cfg/*.conf "${NGINX_CONFIGURATION_PATH}" 
ADD *.html ./ 
ADD password.txt /mnt/config/password.txt

# Run script uses standard ways to run the application
#CMD nginx -g "daemon off;"

CMD [envsubst '$${GITLAB_TOKEN}' < /etc/nginx/conf.d/my-site.conf.template > ${NGINX_CONF_PATH} && nginx -g 'daemon off;']
